﻿#include <stdio.h>
#include<windows.h>
#include<math.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double x1, x2, a, b, c, d;
	printf("================================\n");
	printf("a=");
	scanf_s("%lf", &a);
	printf("b=");
	scanf_s("%lf", &b);
	printf("c=");
	scanf_s("%lf", &c);
	printf("================================\n");
	printf("Рівняння %.0fx*x+%.0fx+%.0f=0\n", a, b, c);
	printf("================================\n");
	d = (b * b - 4 * a * c);
	    if (d > 0)
		{
			x1 = (-b - sqrt(d)) / (2 * a);
			x2 = (-b + sqrt(d)) / (2 * a);

			printf("x1=%.2f\nx2=%.2f\n", x1, x2);
		}
		else if (d == 0)
		{
			x1 = -b / (2 * a);
			printf("x1=%.2f\n", x1);
		}
		else 
		printf("Розв'язків немає\n");
		printf("================================\n");
        return 0;
}
