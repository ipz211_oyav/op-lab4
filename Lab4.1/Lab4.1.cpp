﻿#include<stdio.h>
#include<windows.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double a, b, c;
	printf("Введіть сторони А В С \n");
	scanf_s("%lf%lf%lf", &a, &b, &c);
	if (a + b > c && a + c > b && b + c > a)
	{

		if (a == b && b == c)
		printf("Трикутник рівносторонній\n");
		else if (a == b||a==c||b==c)
		printf("Трикутник рівнобедрений\n");
	      else printf("Трикутник різносторонній\n");
	}
	else printf("З введених сторін не можливо скласти трикутник\n");
	return 0;
}
